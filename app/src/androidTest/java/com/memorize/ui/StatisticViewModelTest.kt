package com.memorize.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.memorize.AppActivity
import com.memorize.data.GroupMockDataSource
import com.memorize.data.Session
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.observeOnce
import com.memorize.ui.groups.GroupViewModel
import com.memorize.ui.statistic.StatisticViewModel
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class StatisticViewModelTest {

    private lateinit var viewModel: StatisticViewModel
    @get:Rule
    var activityRule: ActivityTestRule<AppActivity> = ActivityTestRule(AppActivity::class.java)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init(){
        viewModel = StatisticViewModel(activityRule.activity.application)
    }

    /**
     * Basistest fürs lesen der Daten und aufrufen der benötigten funktionen
     */
    @Test
    fun testModel(){
        val session = Session()
        viewModel.prepare(session) //leere session
        viewModel.hasData.observeOnce {
            assertFalse(it)
        }

        session.incRightAnswer(1L)
        session.incWrongAnswer(1L)
        viewModel.prepare(session) //gefüllte session
        viewModel.hasData.observeOnce {
            assertTrue(it)
        }
    }


}