package com.memorize.ui.opentask

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.memorize.InjectionBase
import com.memorize.R
import com.memorize.databinding.OpenTaskFragmentBinding
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import android.os.Environment
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import android.provider.MediaStore
import android.util.Log
import androidx.core.content.FileProvider
import java.io.IOException


class OpenTaskFragment : Fragment() {

    private lateinit var binding: OpenTaskFragmentBinding
    val args: OpenTaskFragmentArgs by navArgs()
    private var currentPhotoPath: String? = null

    companion object {
        fun newInstance() = OpenTaskFragment()
        private const val REQUEST_SELECT_IMAGE_IN_ALBUM = 1
        private const val REQUEST_PICTURE_CAPTURE = 2
        private const val TAG = "OpenTaskFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.open_task_fragment, container, false)
        binding = DataBindingUtil.bind(view)!!
        binding.viewmodel = ViewModelProviders.of(this, InjectionBase.provideOpenTaskViewModelFactory(this.activity!!.application, args.groupId, args.taskId))
                .get(OpenTaskViewModel::class.java)
        binding.setLifecycleOwner(this)
        binding.viewmodel!!.finished.observe(this, Observer {
            findNavController().popBackStack()
        })
        setHasOptionsMenu(true)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.open_task_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.add_image -> {
                selectImageInAlbum()
                true
            }
            item.itemId == R.id.take_image -> {
                takeImage()
                true
            }
            item.itemId == R.id.delete_task ->{
                binding.viewmodel!!.deleteCurrentTask()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun takeImage() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(activity!!.packageManager)?.also {
                val photoFile: File? = try {
                    createPictureFile()
                } catch (ex: IOException) {
                    Log.e(TAG, ex.message)
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                            context!!,
                            "com.memorize.fileprovider",
                            it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_PICTURE_CAPTURE)
                }
            }
        }
    }

    private fun createPictureFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss", Locale.GERMAN).format(Date())
        val pictureFile = "memorize_capture$timeStamp"
        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(pictureFile, ".jpg", storageDir).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun selectImageInAlbum() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "image/*"
        if (intent.resolveActivity(activity!!.packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){

            if (requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM){
                context!!.contentResolver.takePersistableUriPermission(data!!.data!!, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                val imageUri = data.data!!.toString()
                setPictureByPath(imageUri)
            }else if (requestCode == REQUEST_PICTURE_CAPTURE ){
                setPictureByPath(currentPhotoPath!!)
            }
        }
    }

    private fun setPictureByPath(path: String){
        val current = binding.viewmodel!!.task.value!!
        current.image = path
        binding.viewmodel!!.task.value = current
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.title = getString(R.string.new_task)
    }
}
