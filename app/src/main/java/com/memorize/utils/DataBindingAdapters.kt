package com.memorize.utils

import androidx.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import com.google.android.material.textfield.TextInputLayout
import com.memorize.R
import com.squareup.picasso.Picasso
import java.io.File

/**
 * Helferdatei welches Funtkionen zum anbinden von views and livedata verwendet wird.
 * Hintergrund hierfür ist unter anderem, dass ein Task nur einen Pfad zum Bild beinhaltet, das Binding aber standartmaesig
 * ein Drawable erwartet. Mit der Annotation @BindingAdapter kann so das Bild auch
 * geladen werden, wenn das BindableField ein String also ein Pfad ist
 */

@BindingAdapter("imageResource")
fun setImageUri(view: ImageView, imageUri: String?) {
    if (imageUri == null || imageUri == "") {
        view.setImageURI(null)
    } else {
        if (MediaUtils.isInternalContent(imageUri)){
            Picasso.get().load(imageUri).into(view)
        }else{
            Picasso.get().load(File(imageUri)).into(view)
        }

    }
}

@BindingAdapter("errorText")
fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {
    if (errorMessage != "") {
        view.isErrorEnabled = true
        view.error = errorMessage
    } else {
        view.error = ""
        view.isErrorEnabled = false
    }

}

