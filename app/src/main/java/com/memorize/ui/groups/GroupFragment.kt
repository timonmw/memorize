package com.memorize.ui.groups

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.memorize.InjectionBase

import com.memorize.R
import com.memorize.data.groups.GroupWithTasks
import com.memorize.utils.SubtitleItem
import com.memorize.utils.SubtitleListAdapter
import kotlinx.android.synthetic.main.group_fragment.view.*
import androidx.recyclerview.widget.DividerItemDecoration
import android.widget.LinearLayout.HORIZONTAL
import android.widget.LinearLayout.VERTICAL
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.memorize.data.groups.Group


class GroupFragment : Fragment(), SubtitleListAdapter.ItemSelectedCallback<GroupWithTasks>,
        SubtitleListAdapter.ItemMenuSelectedCallback<GroupWithTasks> {

    private val adapter = SubtitleListAdapter(this)
    private lateinit var viewModel: GroupViewModel

    companion object {
        fun newInstance() = GroupFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.group_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.groupRecyclerView.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        adapter.optionsMenuCallback = this
        view.groupRecyclerView.adapter = adapter
        val itemDecor = DividerItemDecoration(activity!!, VERTICAL)
        view.groupRecyclerView.addItemDecoration(itemDecor)
        view.newGroupButton.setOnClickListener { showNewGroupDialog() }
        activity!!.title = getString(R.string.groups)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!, InjectionBase.provideGroupViewModelFactory(this.activity!!.application))
                .get(GroupViewModel::class.java)
        viewModel.groupsLiveData.observe(this, Observer { it -> updateList(it) })
        viewModel.openGroupEvent.observe(this, Observer { groupId ->
            val action = GroupFragmentDirections.actionGroupFragmentToTaskFragment(groupId)
            findNavController().navigate(action)})
    }

    private fun updateList(list: List<GroupWithTasks>?) {
        val uiList = ArrayList<SubtitleItem<GroupWithTasks>>()

        list?.forEach {
            val subTitleBuilder = StringBuilder(getString(R.string.group_subtitle_prefix))
            uiList.add(SubtitleItem(it.group!!.name,
                    subTitleBuilder.append(" - ").append(it.tasks.size).toString(), it))
        }
        adapter.submitList(uiList)
    }

    override fun onItemSelected(v: View, item: GroupWithTasks) {
        viewModel.openGroupEvent.value = item.group!!.id
    }

    private fun showNewGroupDialog() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.new_group_title))
        builder.setMessage(getString(R.string.new_grop_subtitle))

        val inputText = EditText(activity)
        builder.setView(inputText)
        builder.setPositiveButton(getString(R.string.save)) { _, _ ->
            viewModel.newGroup(inputText.text.toString())
        }
        builder.setNegativeButton(R.string.cancel, null)

        val dialog: AlertDialog = builder.create()

        dialog.show()
    }

    private fun showEditGroupDialog(group: Group) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.edit_group_title))
        builder.setMessage(getString(R.string.edit_grop_subtitle))

        val inputText = EditText(activity)
        inputText.setText(group.name)
        builder.setView(inputText)
        builder.setPositiveButton(getString(R.string.save)) { _, _ ->
            group.name = inputText.text.toString()
            viewModel.saveGroup(group)
        }
        builder.setNegativeButton(R.string.cancel, null)

        val dialog: AlertDialog = builder.create()

        dialog.show()
    }

    override fun onEdit(item: GroupWithTasks) {
        showEditGroupDialog(item.group!!)
    }

    override fun onDelete(item: GroupWithTasks) {
        viewModel.deleteGroup(item.group!!)
    }


}
