package com.memorize.utils

data class SubtitleItem<T>(val title: String,
                           val subtitle: String,
                           val item: T)