package com.memorize.ui.quiz

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.memorize.data.tasks.TaskDataSource

class QuizViewModelFactory (
        private val application: Application,
        private val taskDataSource: TaskDataSource,
        private val groupId: Long)
: ViewModelProvider.NewInstanceFactory() {

    /**
     * Erzeugt eine Instanz eines quiz View Models
     * da von Hand die richtigkeit der Klasse überprüft wird,
     * kann UNCHECKED_CAST unterdrückt werden
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(QuizViewModel::class.java) ->
                        QuizViewModel(application, taskDataSource, groupId)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T
}