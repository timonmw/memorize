package com.memorize.ui.statistic

import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import com.github.mikephil.charting.data.Entry
import com.memorize.R
import kotlinx.android.synthetic.main.statistic_fragment.*
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.data.PieData
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import com.github.mikephil.charting.components.Legend
import androidx.databinding.adapters.TextViewBindingAdapter.setTextSize
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.Description
import com.memorize.InjectionBase
import com.memorize.ui.quiz.QuizViewModel
import java.nio.file.Files.size



class StatisticFragment : Fragment(){

    companion object {
        fun newInstance() = StatisticFragment()
    }

    val args: StatisticFragmentArgs by navArgs()
    private lateinit var binding: com.memorize.databinding.StatisticFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.statistic_fragment, container, false)
        binding = DataBindingUtil.bind(view)!!
        //Keine Factory notwendig, da keine Parameter
        binding.viewmodel = ViewModelProviders.of(activity!!).get(StatisticViewModel::class.java)
        binding.setLifecycleOwner(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initChart()
        binding.viewmodel!!.prepare(args.session)
        activity!!.title = getString(R.string.statistics)
    }

    private fun initChart(){
        val yVals = ArrayList<PieEntry>()
        val colors = ArrayList<Int>()



        if (args.session.rightAnswers() > 0) {
            yVals.add(PieEntry(args.session.rightAnswers().toFloat(), getString(R.string.right_answer)))
            colors.add(ContextCompat.getColor(context!!, R.color.green))
        }
        if (args.session.wrongAnswers() > 0) {
            yVals.add(PieEntry(args.session.wrongAnswers().toFloat(), getString(R.string.wrong_answers)))
            colors.add(ContextCompat.getColor(context!!, R.color.red))
        }
        if (args.session.sometimesRightAnswers() > 0) {
            yVals.add(PieEntry(args.session.sometimesRightAnswers().toFloat(), getString(R.string.unsure_answer)))
            colors.add(ContextCompat.getColor(context!!, R.color.yellow))
        }

        val dataSet = PieDataSet(yVals, "")

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors

        // instantiate pie data object now
        val data = PieData(dataSet)

        data.dataSet = dataSet
        data.setValueFormatter(DefaultValueFormatter(0))
        data.setValueTextSize(context!!.resources.getDimension(R.dimen.small_middle_text_size))
        data.setValueTextColor(Color.WHITE)

        piechart.data = data
        piechart.legend.textSize = 17f
        piechart.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        piechart.setEntryLabelTextSize(0f)
        val des = Description()
        des.text = ""
        piechart.description = des
        piechart.legend.isWordWrapEnabled = true
        piechart.isDrawHoleEnabled = false
        // update pie chart
        piechart.invalidate()
    }

}
