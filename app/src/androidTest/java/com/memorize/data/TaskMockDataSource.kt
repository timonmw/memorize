package com.memorize.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.ui.OpenTaskViewModelTest

class TaskMockDataSource: TaskDataSource {


    var lastInsertedTask: Task? = null
    private val source = MutableLiveData<List<Task>>().apply {
        postValue( ArrayList<Task>().apply {
            add(Task(1, "Frage 1", "antwort 1", "", OpenTaskViewModelTest.GROUP_ID))
            add(Task(2, "Frage 2", "antwort 2", "", OpenTaskViewModelTest.GROUP_ID))
            add(Task(3, "Frage 3", "antwort 3", "", OpenTaskViewModelTest.GROUP_ID))
            add(Task(4, "Frage 4", "", "bild 1", OpenTaskViewModelTest.GROUP_ID))
            add(Task(5, "Frage 5", "antwort 4", "", OpenTaskViewModelTest.GROUP_ID))
            add(Task(6, "Frage 5", "antwort 4", "", -1))
            add(Task(7, "Frage 5", "antwort 4", "", -1))

        })
    }

    override fun getAllTasks(): LiveData<List<Task>> {
        return source
    }

    override fun getTaskById(id: Long, callback: TaskDataSource.Callback) {
        source.value!!.forEach {
            if (it.id == id){
                callback.taskLoaded(it)
            }
        }
    }

    override fun getTasksByGroup(groupId: Long): LiveData<List<Task>> {
        val result = MutableLiveData<List<Task>>()
        val list = ArrayList<Task>()
        source.value!!.forEach {
            if (it.groupId == groupId){
                list.add(it)
            }
        }
        result.postValue(list)
        return result
    }

    override fun insertTask(task: Task) {
        lastInsertedTask = task
    }

    override fun deleteTask(task: Task) {
        lastInsertedTask = null
    }



}
