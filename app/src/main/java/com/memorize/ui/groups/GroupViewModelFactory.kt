package com.memorize.ui.groups

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.memorize.data.groups.GroupDataSource

class GroupViewModelFactory(private val application: Application,
                            private val groupDataSource: GroupDataSource) : ViewModelProvider.AndroidViewModelFactory(application) {

    /**
     * Erzeugt eine Instanz eines GroupFragment View Models
     * da von Hand die richtigkeit der Klasse überprüft wird,
     * kann UNCHECKED_CAST unterdrückt werden
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(GroupViewModel::class.java) ->
                        GroupViewModel(application, groupDataSource)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

}