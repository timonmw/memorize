package com.memorize.ui.quiz

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.memorize.data.Session
import com.memorize.data.tasks.LinearTaskChooser
import com.memorize.data.tasks.TaskDataSource
import com.memorize.utils.LiveTaskSwitcher

class QuizViewModel(application: Application,
                    taskDataSource: TaskDataSource,
                    groupId: Long): AndroidViewModel(application) {


    private val tasksLiveData = taskDataSource.getTasksByGroup(groupId)
    val session = Session()
    val task = LiveTaskSwitcher(tasksLiveData, LinearTaskChooser())
    val answerHidden = MutableLiveData<Boolean>().apply { value = true }

    fun know(){
        session.incRightAnswer(task.value!!.id)
        answerHidden.value = true
        task.next()
    }

    fun dontKnow(){
        session.incWrongAnswer(task.value!!.id)
        answerHidden.value = true
        task.next()
    }

    fun showAnswer(){
        answerHidden.value = false
    }

    fun destroySession() {
        session.reset()
    }

}