package com.memorize.ui.statistic

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.memorize.data.Session
import com.memorize.data.tasks.LinearTaskChooser
import com.memorize.data.tasks.TaskDataSource
import com.memorize.utils.LiveTaskSwitcher

class StatisticViewModel(application: Application): AndroidViewModel(application) {

    val hasData = MutableLiveData<Boolean>()

    fun prepare(session: Session){
        hasData.value = session.wrongAnswers() != 0 || session.rightAnswers() != 0
    }

}