package com.memorize.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskChooser


/**
 * Kombiniert, eine sich anpassende Quelle (livedata) mit einem task chooser
 * Dadurch kann, auch wärend sich die zu grunde liegenden tasks sich ändern, weiter abgefragt werden
 * und die neue tasks erscheinen automatisch
 */
class LiveTaskSwitcher(sourceLiveData: LiveData<List<Task>>,
                       private val chooser: TaskChooser): MediatorLiveData<Task>()  {

    init {
        addSource(sourceLiveData) {
            chooser.obtainSource(it)
            value = chooser.nextTask()
        }
    }

    fun next(){
        value = chooser.nextTask()
    }


}