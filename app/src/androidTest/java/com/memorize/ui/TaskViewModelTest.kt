package com.memorize.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.memorize.AppActivity
import com.memorize.data.TaskMockDataSource
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.observeOnce
import com.memorize.ui.groups.GroupViewModel
import com.memorize.ui.opentask.OpenTaskViewModel
import com.memorize.ui.tasks.TaskViewModel
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class TaskViewModelTest {

    private lateinit var tasksViewModel: TaskViewModel
    private var mockDataSource =  TaskMockDataSource()

    companion object {
        const val GROUP_ID = 300L
    }

    @get:Rule
    var activityRule: ActivityTestRule<AppActivity> = ActivityTestRule(AppActivity::class.java)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init(){
        tasksViewModel = TaskViewModel(activityRule.activity.application, mockDataSource, GROUP_ID)
    }

    /**
     * Basistest fürs lesen der Daten und aufrufen der benötigten funktionen
     */
    @Test
    fun testTasksViewModel(){
        tasksViewModel.tasksLiveData.observeOnce {
            assertTrue(it.size == 5) // 5 gültige einträge im test datenset zu der gruppe
            assertTrue(it[3].image.isNotEmpty())
        }

        val taskId = 3L
        tasksViewModel.openTaskEvent.observeOnce {
            assertTrue(it == taskId)
        }
        tasksViewModel.openTaskEvent.value = taskId
    }

}