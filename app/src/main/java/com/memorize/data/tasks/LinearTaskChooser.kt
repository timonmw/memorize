package com.memorize.data.tasks

class LinearTaskChooser: TaskChooser {

    private var index = 0
    private var source: List<Task>? = null

    override fun obtainSource(source: List<Task>) {
        this.source = source
    }

    override fun nextTask(): Task? {
        return if (source!!.isNotEmpty()){
            if (index == source!!.size){
                index = 0
            }
            val nextTask = source!![index]
            index = index.inc()
            nextTask
        }else{
            null
        }
    }
}