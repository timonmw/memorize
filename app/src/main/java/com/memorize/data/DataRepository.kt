package com.memorize.data

import androidx.lifecycle.LiveData
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDao
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDao
import com.memorize.data.tasks.TaskDataSource
import kotlinx.coroutines.*

/**
 * Repository um den Zugriff auf alle Daten zu verbergen und einfaches
 * Caching und welches zwischen online und offline Daten zu ermöglichen
 * Aufgrund des simplen Umfangs der App nur ein einzelnes Repository für alle Daten
 * um eine höhere skalierbarkeit zu erhalten, muss das Repository nach Datentyp aufgeteilt werden
 */
class DataRepository private constructor(private val taskDao: TaskDao,
                                         private val groupDao: GroupDao): TaskDataSource, GroupDataSource {


    override fun getTaskById(id: Long, callback: TaskDataSource.Callback){
        GlobalScope.launch(Dispatchers.Main) {
            val result = async(Dispatchers.IO) {
                return@async taskDao.getTaskById(id)
            }.await()
            callback.taskLoaded(result)
        }
    }

    override fun getTasksByGroup(groupId: Long): LiveData<List<Task>> = taskDao.getTasksByGroupId(groupId)

    override fun getAllTasks() = taskDao.getAll()

    override fun insertTask(task: Task) {
        GlobalScope.launch(Dispatchers.Main) {
            async(Dispatchers.IO) {
                return@async taskDao.insert(task)
            }.await()
        }
    }

    override fun deleteTask(task: Task) {
        GlobalScope.launch(Dispatchers.Main) {
            async(Dispatchers.IO) {
                return@async taskDao.delete(task)
            }.await()
        }
    }


    override fun getAllGroups() = groupDao.getAll()

    override fun insertGroup(group: Group) {
        GlobalScope.launch(Dispatchers.Main) {
            async(Dispatchers.IO) {
                return@async groupDao.insert(group)
            }.await()
        }
    }

    override fun updateGroup(group: Group) {
        GlobalScope.launch(Dispatchers.Main) {
            async(Dispatchers.IO) {
                return@async groupDao.update(group)
            }.await()
        }
    }

    override fun deleteGroup(group: Group) {
        GlobalScope.launch(Dispatchers.Main) {
            async(Dispatchers.IO) {
                return@async groupDao.delete(group)
            }.await()
        }
    }


    companion object {
        @Volatile private var instance: DataRepository? = null

        fun getInstance(quoteDao: TaskDao, groupDao: GroupDao) =
                instance ?: synchronized(this) {
                    instance
                            ?: DataRepository(quoteDao, groupDao).also { instance = it }
                }
    }
}