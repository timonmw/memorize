package com.memorize.ui.opentask

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.memorize.data.tasks.TaskDataSource

class OpenTaskViewModelFactory (
        private val application: Application,
        private val taskDataSource: TaskDataSource,
        private val groupId: Long,
        private val taskId: Long?)
: ViewModelProvider.NewInstanceFactory() {

    /**
     * Erzeugt eine Instanz eines Task View Models
     * da von Hand die richtigkeit der Klasse überprüft wird,
     * kann UNCHECKED_CAST unterdrückt werden
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(OpenTaskViewModel::class.java) ->
                        OpenTaskViewModel(application, taskDataSource, groupId, taskId)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T
}