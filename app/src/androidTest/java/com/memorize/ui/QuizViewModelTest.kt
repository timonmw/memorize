package com.memorize.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.memorize.AppActivity
import com.memorize.data.TaskMockDataSource
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.observeOnce
import com.memorize.ui.groups.GroupViewModel
import com.memorize.ui.quiz.QuizViewModel
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class QuizViewModelTest {

    private lateinit var viewModel: QuizViewModel
    private var mockDataSource =  TaskMockDataSource()

    companion object {
        const val GROUP_ID = 300L
    }

    @get:Rule
    var activityRule: ActivityTestRule<AppActivity> = ActivityTestRule(AppActivity::class.java)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init(){
        viewModel = QuizViewModel(activityRule.activity.application, mockDataSource, GROUP_ID)
    }

    /**
     * Basistest fürs lesen der Daten und aufrufen der benötigten funktionen
     */
    @Test
    fun testModel(){
        assertTrue(viewModel.answerHidden.value == true) //antwort sollte defaultmäßg nicht zu sehen sein (== true wegen nullbarkeit)
        var lastTask: Task? = null
        viewModel.task.observeOnce {
            assertTrue(viewModel.task.value != null) // es sollte direkt ein task verfügbar sein
            lastTask = it
        }
        viewModel.showAnswer()
        viewModel.answerHidden.observeOnce {
            assertFalse(it) // antwort sollte zu sehen sein
        }

        viewModel.know()
        viewModel.task.observeOnce {
            assertTrue(lastTask != it) // es sollte ein weitere Task anzeiegt werden
            lastTask = it
        }

        viewModel.dontKnow()
        viewModel.task.observeOnce {
            assertTrue(lastTask != it) // es sollte ein weitere Task anzeiegt werden
        }

        // anzahl der antworten überprüfen
        assertTrue(viewModel.session.rightAnswers() == 1)
        assertTrue(viewModel.session.wrongAnswers() == 1)
    }

}