package com.memorize.ui.groups

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.utils.LiveEvent

class GroupViewModel(application: Application,
                     private val groupDataSource: GroupDataSource): AndroidViewModel(application) {

    var groupsLiveData: LiveData<List<GroupWithTasks>> = groupDataSource.getAllGroups()
    val openGroupEvent = LiveEvent<Long>()

    fun newGroup(name: String){
        val newGroup = Group(name = name)
        groupDataSource.insertGroup(newGroup)
    }

    fun saveGroup(group: Group){
        groupDataSource.updateGroup(group)
    }

    fun deleteGroup(group: Group){
        groupDataSource.deleteGroup(group)
    }
}