package com.memorize.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.memorize.AppActivity
import com.memorize.data.GroupMockDataSource
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.observeOnce
import com.memorize.ui.groups.GroupViewModel
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class GroupViewModelTest {

    private lateinit var viewModel: GroupViewModel
    private var mockDataSource =  GroupMockDataSource()

    @get:Rule
    var activityRule: ActivityTestRule<AppActivity> = ActivityTestRule(AppActivity::class.java)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init(){
        viewModel = GroupViewModel(activityRule.activity.application, mockDataSource)
    }

    /**
     * Basistest fürs lesen der Daten und aufrufen der benötigten funktionen
     */
    @Test
    fun testModel(){
        viewModel.groupsLiveData.observeOnce {
            assertTrue(it.size == 5)
            assertTrue(it[4].tasks.isNotEmpty())
        }

        val testName = "name"
        viewModel.newGroup(testName)
        assertTrue(mockDataSource.lastAddedGroup!!.name == testName)

        val groupInDb = mockDataSource.lastAddedGroup!!.copy()
        val newName = "new name"
        groupInDb.name = newName
        viewModel.saveGroup(groupInDb)
        assertTrue(mockDataSource.lastAddedGroup!!.name == newName &&
                   mockDataSource.lastAddedGroup!!.id == groupInDb.id
        )// neuer name vergeben aber id sollte sich nicht geändert haben

        viewModel.deleteGroup(mockDataSource.lastAddedGroup!!)
        assertTrue(mockDataSource.lastAddedGroup == null)

        val testId = 30L
        viewModel.openGroupEvent.observeOnce {
            assertTrue(it == testId)
        }
        viewModel.openGroupEvent.value = testId

    }


}