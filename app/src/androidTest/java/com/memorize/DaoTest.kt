package com.memorize

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.memorize.data.AppDatabase
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDao
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDao
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class DaoTest {

    private lateinit var taskDao: TaskDao
    private lateinit var groupDao: GroupDao
    private lateinit var db: AppDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
                context, AppDatabase::class.java).build()
        taskDao = db.taskDao()
        groupDao = db.groupDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun saveAndReadTaskAndGroup() {
        //gruppe fuer den Task erstellen
        val group = Group(name = "TestGroup")
        group.id  = groupDao.insert(group)

        //ist die gruppe in der Datenbank?
        groupDao.getAll().observeOnce {
            val groupIsInDb = it.stream().anyMatch{ item -> item.group!!.id == group.id}
            assertTrue(groupIsInDb)
        }
        //task in die gruppe speichern
        val task = Task(question = "", answer = "", groupId = group.id, image = "")
        task.id = taskDao.insert(task)

        //it der task in der Datenbank?
        taskDao.getAll().observeOnce {
            val isInDatabase = it.contains(task)
            assertTrue(isInDatabase)
        }


    }


}