package com.memorize.ui.quiz

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.memorize.AppActivity
import com.memorize.InjectionBase
import com.memorize.R
import com.memorize.data.tasks.Task
import com.memorize.databinding.OpenTaskFragmentBinding
import com.memorize.utils.Constants.Companion.NO_ID
import com.memorize.utils.SubtitleItem
import com.memorize.utils.SubtitleListAdapter
import kotlinx.android.synthetic.main.quiz_fragment.*
import kotlinx.android.synthetic.main.task_fragment.*

class QuizFragment : Fragment(){

    companion object {
        fun newInstance() = QuizFragment()
    }

    val args: QuizFragmentArgs by navArgs()
    private lateinit var binding: com.memorize.databinding.QuizFragmentBinding



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.quiz_fragment, container, false)
        binding = DataBindingUtil.bind(view)!!
        binding.viewmodel = ViewModelProviders.of(activity!!, InjectionBase.provideQuizViewModelFactory(this.activity!!.application, args.groupId))
                .get(QuizViewModel::class.java)
        binding.setLifecycleOwner(this)
        setHasOptionsMenu(true)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.quiz_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.statistics -> {
                val action = QuizFragmentDirections.actionQuizFragmentToStatisticFragment(binding.viewmodel!!.session)
                findNavController().navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.title = getString(R.string.training)
    }

    override fun onDetach() {
        super.onDetach()
        binding.viewmodel!!.destroySession()
    }
}
