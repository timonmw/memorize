package com.memorize.data.groups

import androidx.lifecycle.LiveData

interface GroupDataSource {
    fun getAllGroups(): LiveData<List<GroupWithTasks>>
    fun insertGroup(group: Group)
    fun updateGroup(group: Group)
    fun deleteGroup(group: Group)
}