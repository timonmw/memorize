package com.memorize.ui.tasks

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.memorize.InjectionBase
import com.memorize.R
import com.memorize.data.tasks.Task
import com.memorize.utils.Constants.Companion.NO_ID
import com.memorize.utils.SubtitleItem
import com.memorize.utils.SubtitleListAdapter
import kotlinx.android.synthetic.main.task_fragment.*

class TaskFragment : Fragment(), SubtitleListAdapter.ItemSelectedCallback<Task> {

    companion object {
        fun newInstance() = TaskFragment()
    }

    private lateinit var viewModel: TaskViewModel
    private lateinit var startButton: MenuItem
    private val adapter = SubtitleListAdapter(this)
    val args: TaskFragmentArgs by navArgs()



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.task_fragment, container, false)
    }

    override fun onItemSelected(v: View, item: Task) {
        viewModel.openTaskEvent.value = item.id
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        taskRecyclerView.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        taskRecyclerView.adapter = adapter
        val itemDecor = DividerItemDecoration(activity!!, LinearLayout.VERTICAL)
        taskRecyclerView.addItemDecoration(itemDecor)
        newTaskButton.setOnClickListener {
            viewModel.openTaskEvent.call()
        }
        setHasOptionsMenu(true)
        activity!!.title = getString(R.string.tasks)
    }

    private fun updateList(list: List<Task>?) {
        val uiList = ArrayList<SubtitleItem<Task>>()

        list?.forEach {
            var answer = it.answer
            if (it.answer == "" && it.image != ""){
                answer = getString(R.string.image_answer_only)
            }
            uiList.add(SubtitleItem(it.question, answer, it))
        }
        adapter.submitList(uiList)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!, InjectionBase.provideTaskViewModelFactory(this.activity!!.application, args.groupId))
                .get(TaskViewModel::class.java)
        viewModel.tasksLiveData.observe(this, Observer { updateList(it) })
        viewModel.openTaskEvent.observe(this, Observer { taskId ->
            if (taskId == null){
                val action = TaskFragmentDirections.actionTaskFragmentToOpenTaskFragment(NO_ID, args.groupId)
                findNavController().navigate(action)
            }else{
                val action = TaskFragmentDirections.actionTaskFragmentToOpenTaskFragment(taskId, args.groupId)
                findNavController().navigate(action)
            }

        })
        viewModel.openQuizEvent.observe(this, Observer {
            val action = TaskFragmentDirections.actionTaskFragmentToQuizFragment(args.groupId)
            findNavController().navigate(action)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.tasks_menu, menu)
        startButton = menu.findItem(R.id.start)
        viewModel.tasksLiveData.observe(this, Observer {
            startButton.isVisible = !it.isNullOrEmpty()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.start -> {
                viewModel.beginQuiz()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }



}
