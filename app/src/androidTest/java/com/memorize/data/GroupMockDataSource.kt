package com.memorize.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.data.tasks.Task
import java.util.*

class GroupMockDataSource : GroupDataSource {

    private val source = MutableLiveData<List<GroupWithTasks>>().apply {
        postValue( ArrayList<GroupWithTasks>().apply {
            add(GroupWithTasks(Group(1, "group 1"), ArrayList()))
            add(GroupWithTasks(Group(2, "group 2"), ArrayList()))
            add(GroupWithTasks(Group(3, "group 3"), ArrayList()))
            add(GroupWithTasks(Group(4, "group 4"), ArrayList()))
            //eine Group mit 3 Tasks
            add(GroupWithTasks(Group(5, "group 5"), ArrayList<Task>().apply {
                add(Task(-1, "", "", "", -1))
                add(Task(-1, "", "", "", -1))
                add(Task(-1, "", "", "", -1))
            }))
        })
    }

    var lastAddedGroup: Group? = null

    override fun getAllGroups(): LiveData<List<GroupWithTasks>> {
        return source
    }

    /**
     * Faked das Speichern der Group. Falls die Goup bereits gespeichert wurde werden
     * nur die Werte übernommen, falls nicht wird auch eine neue id generiert
     */
    override fun insertGroup(group: Group) {
        lastAddedGroup = group  //"datenbank" ist leer -> neu hinzufuegen und id generieren
        lastAddedGroup!!.id = System.currentTimeMillis() //nicht 100% eindeutig aber fürs testen ausreichend
    }

    override fun updateGroup(group: Group) {
        if (group.id == lastAddedGroup!!.id){ //muss schon vorhanden sein zum update
            lastAddedGroup = group
        }
    }

    override fun deleteGroup(group: Group) {
        lastAddedGroup = null
    }

}