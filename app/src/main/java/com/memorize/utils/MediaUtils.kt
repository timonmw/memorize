package com.memorize.utils

import android.net.Uri
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import java.io.File
import java.util.*


object MediaUtils {

    fun generateImagePath(): Uri{
        return Uri.fromFile(File(getExternalStorageDirectory(),
                UUID.randomUUID().toString() + ".jpg"))
    }

    fun isInternalContent(path: String): Boolean{
        return path.startsWith("content://")
    }
}