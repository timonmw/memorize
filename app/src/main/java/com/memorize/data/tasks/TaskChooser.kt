package com.memorize.data.tasks

interface TaskChooser {
    fun obtainSource(source: List<Task>)
    fun nextTask(): Task?
}