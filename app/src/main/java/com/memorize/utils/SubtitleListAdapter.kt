package com.memorize.utils


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.memorize.R
import androidx.appcompat.widget.PopupMenu


/**
 * Generische klasse für listen, die Items mit title und untertitel anzeigt
 * Optional kann ein Menu callback gesetzt werden um ein Options menu für jedes Item T anzuzeigen
 */
class SubtitleListAdapter<T> (private val itemSelectedCallback: ItemSelectedCallback<T>): ListAdapter<SubtitleItem<T>, SubtitleListAdapter.ItemViewHolder<T>>(DiffCallback<T>()) {

    var optionsMenuCallback: ItemMenuSelectedCallback<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<T> {
        return ItemViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_with_subtitle, parent, false),
                itemSelectedCallback,
                optionsMenuCallback
        )
    }

    override fun onBindViewHolder(holder: SubtitleListAdapter.ItemViewHolder<T>, position: Int) {
        holder.bind(getItem(position))
    }

    class ItemViewHolder<T>(itemView: View,private val itemSelectedCallback: ItemSelectedCallback<T>,
                            private val itemMenuSelectedCallback: ItemMenuSelectedCallback<T>?) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: SubtitleItem<T>) = with(itemView) {
            val title = findViewById<TextView>(R.id.title)
            val subtitle = findViewById<TextView>(R.id.sub_title)
            val menuButton = findViewById<ImageButton>(R.id.menuButton)

            title.text = item.title
            subtitle.text = item.subtitle

            setOnClickListener {
                itemSelectedCallback.onItemSelected(it, item.item)
            }

            if (itemMenuSelectedCallback != null){
                menuButton.setOnClickListener {
                    val popup = PopupMenu(context, menuButton)

                    popup.menuInflater.inflate(com.memorize.R.menu.edit_menu, popup.menu)
                    popup.setOnMenuItemClickListener {
                        when {
                            it.itemId == R.id.edit -> {
                                itemMenuSelectedCallback.onEdit(item.item)
                                true
                            }
                            it.itemId == R.id.delete -> {
                                itemMenuSelectedCallback.onDelete(item.item)
                                true
                            }
                            else -> false
                        }

                    }

                    popup.show()
                }
            }else{
                menuButton.visibility = View.GONE
            }
        }
    }

    class DiffCallback<T> : DiffUtil.ItemCallback<SubtitleItem<T>>() {
        override fun areItemsTheSame(oldItem: SubtitleItem<T>, newItem: SubtitleItem<T>): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: SubtitleItem<T>, newItem: SubtitleItem<T>): Boolean {
            return oldItem.title == newItem.title && oldItem.subtitle == newItem.subtitle
        }
    }

    interface ItemSelectedCallback<T>{
        fun onItemSelected(v: View, item: T)
    }

    interface ItemMenuSelectedCallback<T>{
        fun onEdit(item: T)
        fun onDelete(item: T)
    }
}

