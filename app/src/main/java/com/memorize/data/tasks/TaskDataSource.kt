package com.memorize.data.tasks

import androidx.lifecycle.LiveData

interface TaskDataSource {
    fun getAllTasks(): LiveData<List<Task>>
    fun getTaskById(id: Long, callback: TaskDataSource.Callback)
    fun getTasksByGroup(groupId: Long): LiveData<List<Task>>
    fun insertTask(task: Task)
    fun deleteTask(task: Task)

    interface Callback{
        fun taskLoaded(task: Task)
    }
}