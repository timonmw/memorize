package com.memorize.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Einfaches Datenobjekt, welches die Informationen über eine quiz session
 * in sich hält. Da nur statistiken für eine aktuelle Runde angezeigt werden, wird
 * dieses objekt nicht in der Datenbank persistiert
 * Dabei wird zwischen richtigen, falschen und manichmal richtigen antworten differenziert
 * Es können richtige und falsche Antworten hinzugefügt werden, wenn eine Antwort manichmal richtig und falsch
 * beantwortet wird, wird sie solange sie unter 2/3 richtig beantwortet wird in "manichmal" richtige antworten
 * kategorisiert
 */
@Parcelize
class Session(private val rightAnswers: ArrayList<Long>,
              private val wrongAnswers: ArrayList<Long>): Parcelable {

    companion object {
        private const val SOMETIMES_RIGHT_PERCENTAGE = 0.66
    }

    constructor() : this(ArrayList<Long>(), ArrayList<Long>())

    fun incRightAnswer(id: Long){
        rightAnswers.add(id)
    }

    fun incWrongAnswer(id: Long){
        wrongAnswers.add(id)
    }

    /**
     * Die normale Anzahl der antworten zurückgeben
     */
    fun rightAnswers():Int = rightAnswers.size
    fun wrongAnswers():Int = wrongAnswers.size

    /**
     * Errechnen wie viele der richtigen Antworten auch schon falsch
     * beantwortet wurden und schauen wie oft diese falsch und richtig waren
     * wenn sie mit weniger als 66% richtig waren werden sie in die statistik
     * der manichmal richtigen mitaufgenommen
     */
    fun sometimesRightAnswers(): Int{
        var result = 0
        rightAnswers.forEach {
            val rightCount = countInList(it, rightAnswers).toFloat()
            val wrongCount = countInList(it, wrongAnswers).toFloat()
            if (rightCount > 0){
                if (wrongCount/rightCount >= SOMETIMES_RIGHT_PERCENTAGE){
                    result = result.inc()
                }
            }

        }
        return result
    }

    private fun countInList(id: Long, list: List<Long>): Int{
        var result = 0
        list.forEach {
            if (it == id) result = result.inc()
        }
        return result
    }

    fun reset() {
        rightAnswers.clear()
        wrongAnswers.clear()
    }

}