package com.memorize.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDao
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDao

/**
 * Datenbank Klasse zum erstellen und verwalten der Datenbank
 * Das einfügen der DAOs wird von Room übernommen
 * Die Datenbank wird als Thread sicherer Singleton verwaltet um nur
 * eine Instanz der Datenbank zu erzeugen
 */
@Database(entities = [Task::class, Group::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun groupDao(): GroupDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java, "memorize.db").build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}
