# Memorize

### Anforderungen

Die App besteht aus 5 Screens:

 - Die Karteikarten sollen Gruppiert werden können in z.B. Fächer wie Mathe oder Recht. Diese Gruppen sollen gelöscht und umbennant werden können.
 - Jede Gruppe beinhaltet ein Set aus Karteikarten. Diese Karten können in der Gruppe hinzugefügt, bearbeitet und gelöscht werden können.
 - Eine Karteikarte bzw. eine Aufgabe besteht aus einer Frage mit einer Antwort. Als Antwort kann Text, Bild oder beides angegeben werden. Bilder können direkt aus der Kamera aufgenommen werden können oder aus der Galerie ausgewählt werden.
 - Die Karteikarten können pro Gruppe Abgefragt werden. Bei der Abfrage wird zuerst die Frage angezeigt und die Antwort verborgen. Die Antwort kann auf wunsch eingeblendet werden. Zusätzlich kann ausgewählt werden ob man die Antwort gewusst oder nicht gewusst hat.
 - Beim Abfragen soll ein weiterer Screen abgerufen werden können, der Statistiken über die momentane Abfrage anzeigt und einem Feedback dazu gibt wie sicher man das Set der Karteikarten beantwortet hat

#### Architektur

Die App verwendet die MVVM-Architektur. Zusätzlich wird Android-Navigation-Component zu Navigation, Viewbinding, Room und AndroidX eingesetzt. Zur Qualitätssicherung wird eine Bitbucket Pipeline eingesetzt.


#### Tests

Die Test der App sichern die Basisfunktionalitäten der Datenbank und der Businesslogik ab. Dabei werden die Test von Datenbank und Businesslogik voneinander getrennt um möglichst kegapselte Testergebnisse zu erhalten.

### Externe Bibliotheken
 - https://github.com/PhilJay/MPAndroidChart
 - https://github.com/square/picasso

### Verwendete Versionen

Android Studio: 3.3
Kotlin-Plugin: 1.3.20-release
Grandle: 4.10.1
Android-Plugin: 3.3.0

