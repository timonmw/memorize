package com.memorize.data.groups

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "groups")
data class Group(@PrimaryKey(autoGenerate = true) var id: Long = 0,
                 @ColumnInfo(name = "name") var name: String)
