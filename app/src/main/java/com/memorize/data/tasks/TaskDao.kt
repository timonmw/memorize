package com.memorize.data.tasks

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

/**
 * DAO Interface fürs Speichern und Löschen von Tasks
 */
@Dao
interface TaskDao {

    @Query("SELECT * from tasks")
    fun getAll(): LiveData<List<Task>>

    @Insert(onConflict = REPLACE)
    fun insert(task: Task): Long

    @Delete
    fun delete(task: Task)

    @Query("SELECT * FROM tasks WHERE group_id=:groupId")
    fun getTasksByGroupId(groupId: Long): LiveData<List<Task>>


    @Query("SELECT * FROM tasks WHERE id=:taskId")
    fun getTaskById(taskId: Long): Task
}