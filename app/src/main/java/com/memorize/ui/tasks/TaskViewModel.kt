package com.memorize.ui.tasks

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.utils.LiveEvent

class TaskViewModel(application: Application,
                    taskDataSource: TaskDataSource,
                    groupId: Long): AndroidViewModel(application) {

    val tasksLiveData = taskDataSource.getTasksByGroup(groupId)
    val openTaskEvent = LiveEvent<Long>()
    val openQuizEvent = LiveEvent<Void>()

    fun beginQuiz() {
        if (!tasksLiveData.value!!.isNullOrEmpty()){
            openQuizEvent.call()
        }

    }

}