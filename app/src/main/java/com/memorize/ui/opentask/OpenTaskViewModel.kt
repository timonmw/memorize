package com.memorize.ui.opentask

import android.app.Application
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.memorize.R
import com.memorize.data.tasks.Task
import com.memorize.data.DataRepository
import com.memorize.data.tasks.TaskDataSource
import com.memorize.utils.Constants.Companion.NO_ID
import com.memorize.utils.LiveEvent

class OpenTaskViewModel(private val app: Application,
                        private val taskDataSource: TaskDataSource,
                        val groupId: Long,
                        val taskId: Long?): AndroidViewModel(app), TaskDataSource.Callback {

    val task = MutableLiveData<Task>().apply {
        value = (Task(question = "", answer = "", image = "", groupId = groupId))
        if (taskId != null && taskId != NO_ID){
            taskDataSource.getTaskById(taskId, this@OpenTaskViewModel)
        }
    }
    val questionError = MutableLiveData<String>()
    val answerError = MutableLiveData<String>()
    val finished = LiveEvent<Void>()

    override fun taskLoaded(task: Task) {
        this.task.value = task
    }

    fun saveTask(){
        var valid = true
        if (task.value!!.question == ""){
            questionError.value = app.getString(R.string.question_needed)
            valid = false
        }else{
            questionError.value = ""
        }
        if (task.value!!.answer == "" && task.value!!.image == "" ){
            answerError.value = app.getString(R.string.answer_needed)
            valid = false
        }else{
            answerError.value = ""
        }

        if (valid){
            taskDataSource.insertTask(task.value!!)
            finished.call()
        }
    }

    fun deleteCurrentTask() {
        taskDataSource.deleteTask(task.value!!)
        finished.call()
    }

}