package com.memorize.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.memorize.AppActivity
import com.memorize.data.TaskMockDataSource
import com.memorize.data.groups.Group
import com.memorize.data.groups.GroupDataSource
import com.memorize.data.groups.GroupWithTasks
import com.memorize.data.tasks.Task
import com.memorize.data.tasks.TaskDataSource
import com.memorize.observeOnce
import com.memorize.ui.groups.GroupViewModel
import com.memorize.ui.opentask.OpenTaskViewModel
import com.memorize.ui.tasks.TaskViewModel
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class OpenTaskViewModelTest {

    private lateinit var openTaskViewModel: OpenTaskViewModel
    private var mockDataSource =  TaskMockDataSource()

    companion object {
        const val GROUP_ID = 300L
    }

    @get:Rule
    var activityRule: ActivityTestRule<AppActivity> = ActivityTestRule(AppActivity::class.java)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init(){
        openTaskViewModel = OpenTaskViewModel(activityRule.activity.application, mockDataSource, GROUP_ID, null)
    }

    private fun invalidTest(invalidId: Long, invalidTask: Task){
        //abfragen wann sich der task ändert
        openTaskViewModel.task.value = invalidTask
        openTaskViewModel.task.observeOnce {
            assertTrue(it.id == invalidId)
            //probieren den task zu speichern
            openTaskViewModel.saveTask()
            assertTrue(mockDataSource.lastInsertedTask == null) // darf nicht gespeichert werden
        }
    }

    @Test
    fun testOpenTaskViewModel(){
        assertTrue(openTaskViewModel.task.value != null) // task darf nie null sein, aufgrund des viewbindings

        val invalidTasks = ArrayList<Task>().apply{
            add(Task(1, "", "antwort", "bild", GROUP_ID))
            add(Task(2, "frage", "", "", GROUP_ID))
            add(Task(3, "", "", "bild", GROUP_ID))
            add(Task(4, "sdf", "", "", GROUP_ID))
        }

        invalidTasks.forEach{invalidTest(it.id, it)}

        //probiere den task zu speichern
        val validId = 5L
        val validTask = Task(validId, "sdf", "sdsd", "sdsdd", GROUP_ID)
        openTaskViewModel.task.value = validTask
        openTaskViewModel.task.observeOnce {
            assertTrue(it.id == validId)
            //probieren den task zu speichern
            openTaskViewModel.saveTask()
            assertTrue(mockDataSource.lastInsertedTask != null) // sollte gespeichert sein
        }

        //probiere den task zu löschen
        openTaskViewModel.deleteCurrentTask()
        assertTrue(mockDataSource.lastInsertedTask == null)

    }

}