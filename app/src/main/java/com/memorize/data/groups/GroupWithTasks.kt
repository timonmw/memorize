package com.memorize.data.groups

import androidx.room.Embedded
import androidx.room.Relation
import com.memorize.data.tasks.Task

/**
 * Klasse für die Beziehung zwischen Gruppen und Tasks.
 * Eine Gruppe hat dabei n Tasks
 */
data class GroupWithTasks (@Embedded
                           var group: Group? = null,

                           @Relation(parentColumn = "id", entityColumn = "group_id")
                           var tasks: List<Task> = ArrayList())