package com.memorize

import android.app.Application
import com.memorize.data.AppDatabase
import com.memorize.data.DataRepository
import com.memorize.ui.groups.GroupViewModelFactory
import com.memorize.ui.opentask.OpenTaskViewModelFactory
import com.memorize.ui.quiz.QuizViewModelFactory
import com.memorize.ui.tasks.TaskViewModelFactory

/**
 * Basisklasse für alle in der App verwendeten MVVM/Room Abhänigkkeiten
 * Diese können innerhalb dieses Objektes dynamsich ausgetauscht werden
 */
object InjectionBase {


    /**
     * Erstellt eine Tasks ViewModel Factory mit den für die Produktion benötigten
     * Abhänigkeiten
     */
    fun provideQuizViewModelFactory(application: Application, groupId: Long): QuizViewModelFactory {
        return QuizViewModelFactory (application, getProductionRepository(application), groupId)
    }

    /**
     * Erstellt eine Tasks ViewModel Factory mit den für die Produktion benötigten
     * Abhänigkeiten
     */
    fun provideTaskViewModelFactory(application: Application, groupId: Long): TaskViewModelFactory{
        return TaskViewModelFactory(application, getProductionRepository(application), groupId)
    }

    /**
     * Erstellt eine OepnTask ViewModel Factory mit den für die Produktion benötigten
     * Abhänigkeiten
     */
    fun provideOpenTaskViewModelFactory(application: Application, groupId: Long, taskId: Long?): OpenTaskViewModelFactory{
        return OpenTaskViewModelFactory(application, getProductionRepository(application), groupId, taskId)
    }


    /**
     * Erstellt eine GroupFragment ViewModel Factory mit den für die Produktion benötigten
     * Abhänigkeiten
     */
    fun provideGroupViewModelFactory(application: Application): GroupViewModelFactory{
        return GroupViewModelFactory(application, getProductionRepository(application))
    }

    private fun getProductionRepository(application: Application): DataRepository{
        val taskDao = AppDatabase.getInstance(application)!!.taskDao()
        val groupDao = AppDatabase.getInstance(application)!!.groupDao()
        return DataRepository.getInstance(taskDao, groupDao)
    }
}