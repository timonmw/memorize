package com.memorize.data.groups

import androidx.annotation.TransitionRes
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface GroupDao {

    @Transaction
    @Query("SELECT * from groups")
    fun getAll(): LiveData<List<GroupWithTasks>>

    @Insert(onConflict = REPLACE)
    fun insert(group: Group): Long

    @Update(onConflict = REPLACE)
    fun update(group: Group)

    @Delete
    fun delete(group: Group)

}