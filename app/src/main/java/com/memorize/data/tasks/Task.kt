package com.memorize.data.tasks

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.room.*
import com.memorize.data.groups.Group

/**
 * Datenklasse für eine Frage
 */
@Entity(tableName = "tasks",
        foreignKeys = [ForeignKey(entity = Group::class,
                                            parentColumns = arrayOf("id"),
                                            childColumns = arrayOf("group_id"),
                                            onDelete = ForeignKey.CASCADE)],
        indices = [Index("group_id")]
        )
data class Task(@PrimaryKey(autoGenerate = true) var id: Long = 0,
                @ColumnInfo(name = "question") var question: String,
                @ColumnInfo(name = "answer") var answer: String,
                @ColumnInfo(name = "image") var image: String,
                @ColumnInfo(name = "group_id") var groupId: Long
)

